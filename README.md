# Positive English (en) Language Form

Positive English (en) Language Form , Learning In Progress .

This is a learning in progress project focused primarily with the positive form of communication in the English language .

Positive English Language Form (PELF) is operating as a tentative name . Definition pending ( Focus is positive words ) .

All Feedback and Contributions welcome !

Consideration and Contributions for Positive Language Form (PLF) are welcome here with English and other languages such as [French](https://gitlab.com/plf_language/pfrlf/-/blob/master/README.md) and even others .

# Synopsis

The intention and purpose of this project is create a learning in a progress project based solely on the utilization of the positive form of communication in the English Language as exclusively as possible . We can also call this Positive Exclusivity which is a form of exclusivity intended for the mutual benefit of everyone .

Here we primarily consider communication in , and through , human language in any form is driven essentially by :

* **feelings** ( "Do I and/or others feel good before , during and/or after this communication ? " ) , and

* **intentions** ( " What is my/our intention with this communication ? What is the outcome we intend to achieve ? " )


This project is deeply-rooted and based upon the understanding all human communication languages are designed inherently to encourage both the person and/or people as Speaker and Listener , as ideally as Mutual Communicators (MC) , in mutual communication , to have a positive overall outcome with any , and all , communication , examples of portions are as follows :

* leading into         ( " How is this communication going to go ? ")
* during               ( " I like how I **feel** about this communication ! ") 
* conclusion           ( " Wait ? What ? We have to conclude this communication for now ? Already ? When is our next one ? ")
* and after-conclusion ( " Do I remember this communication as a positive experience , other , or other less than positive experience ? " )

This is important to remember as all communication is experience and feelings based . Intentions fit together in this also .

This is also deeply-root and based upon the application of where each Communicator and/or Listener , as roles can switch often depending upon the communication form and format (for example , lecture/teaching styles differ from other forms typically ) , should feel good about what is being accomplished and all should feel good together .


The primary intention here is utilizing positive only words in the right contexts .

Some contexts require original words and feelings and these should be sparingly included . Communication is driven by intentions and will cause feelings to be changed toward positive or an other than positive often . It is also possible positive only words may seem very effective with certain types of other people , depending upon many other factors , and who may also be less commonly aware of this method of positive-only communication . Patience with everyone is also key .

This also depends deeply upon this situation and all of the related specifics of why original words are necessary . For example , is the intention to quote someone and/or some people directly so original meaning is maintained as close as possible ? Should the answer be yes , it is best to associate this directly as a quote and a generic someone / some people unless specific names are required and/or required to be left omitted for a form of privacy protection reasons .

Example : " The Truth . The whole Truth . Only the Truth. "  ( re-phrased/course-corrected from Legal contexts )

In the examples below , please note words like **kind** are to be understood like [positive infinity](https://en.wikipedia.org/wiki/Infinity) in [Math](https://en.wikipedia.org/wiki/Math) and have a value so high they are unable to be calculated at all and , therefore , we center back to this same word to define and represent itself .

# Examples

**A Few Very Positive Examples** :

* This feels very deeply **kind** to me . I appreciate this deeply . Thank You !
😀🕊🌅

* This feels equal to very **kind** to me . I appreciate this deeply . Thank You !
😀🕊🌅

**A Few Other Than Positive Examples** :

* This feels very **other** than **kind** to me . I feel very deeply hurt .😭

* This feels very much less than **kind** to me . I feel very deeply hurt .😭

**Other Positive Examples** :

* Everyone helping everyone **positively** is awesome .

* Let’s have an awesome positive day all .

# Core Documents

* [Positive Form Core Words](https://gitlab.com/plf_language/pelf/-/blob/master/_positive_form_core_words.md)
* [Positive Form Core Qualifiers](https://gitlab.com/plf_language/pelf/-/blob/master/_positive_form_qualifiers_words.md)
* [Positive Form Core Phrases](https://gitlab.com/plf_language/pelf/-/blob/master/_positive_form_core_phrases.md)
* [Positive Form Feeling Words](https://gitlab.com/plf_language/pelf/-/blob/master/_positive_form_feeling_words.md)

# Related References

These documents will require further review and essentially can help with further ongoing learning in progress with this and related projects . Please note these are included as a possibly supportive documents and are other than necessarily specific endorsements of full content .

* [Positive Words Vocabulary List](https://www.enchantedlearning.com/wordlist/positivewords.shtml)
* [How to Turn 11 Everyday Phrases From Negative to Positive](https://www.happify.com/hd/11-everyday-phrases-negative-to-positive/)
* [Skill Packs - Positive language examples to help your team embrace change](https://www.skillpacks.com/positive-language-examples/)
* [Multilingual - The power of positive language](https://multilingual.com/positive-language/)

* Additional insights and reference documents welcome also .
