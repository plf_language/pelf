# Learning In Progress Positive Form Qualifiers (Sample)

The following is a growing , exacting , and living list of some of the many Positive Form Core Qualifiers .

These terms should be put in applicable places alongside the applicable core positive words .

Some of these are better placed before or after the core positive words depending on specifics of the exact context .

Each of the phrases and/or words , essentially , have the Math equivalent of modifying the value of the [Positive Form Core Words](https://gitlab.com/plf_language/pelf/-/blob/master/_positive_form_core_words.md) . This is an growing , exacting and living list of terms and will be modified over time as well the list below .

<br />

deeply

greater

greater than

greatly

less

less than

much

much greater than

much lesser than

very deeply

very greatly

very much

very much less than

very much greater than
