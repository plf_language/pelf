# Learning In Progress Positive Form Core Words (Sample)

The following is a growing , exacting , and living list of some of the many Positive Form Core Words .

Each of the words , essentially , have the Math equivalent value of [positive infinity](https://en.wikipedia.org/wiki/Infinity) in [Math](https://en.wikipedia.org/wiki/Math) and have a value so high they are unable to be calculated at all .

<br />

application

apply

appreciate

awesome

believe

blessing

build

builder

celebrate

charity

comfort

earth

eden

endorse

endure

enjoy

faith

fantastic

favour

friend

gift

good

goshen

heaven

hope

intend

intent

intention

kind

kindness

lamp

life

light

like

living

love

mighty

patience

peace

pleasure

positive

privilege

prophecy

rejoice

resource

rock

sign

seal

stone

true 

trust

wonderful

worth

worthy
