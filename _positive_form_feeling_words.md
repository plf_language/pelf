# Learning In Progress Positive Form Feeling Words (Sample)

The following is a growing , exacting , and living list of some of the many Positive Form Core Words .

Each of these words , essentially , have the Math equivalent value of [positive infinity](https://en.wikipedia.org/wiki/Infinity) in [Math](https://en.wikipedia.org/wiki/Math) when properly applied and exacted to this correct context , and have a value so high they are unable to be calculated at all and are in relation to expressing **feelings** ("How do I **feel**?").


<br />

comfort / comfortable

disturb / disturbed

friend / friendly

good

glad

grieve

happy

peace / peaceful

positive

relief / relieved

sad

trouble / troubled
